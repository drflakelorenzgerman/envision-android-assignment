package ir.shobeyri.ocr.di

import android.content.Context
import androidx.room.Room
import com.grapesnberries.curllogger.CurlLoggerInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ir.shobeyri.ocr.source.db.DaoInterface
import ir.shobeyri.ocr.source.db.Db
import ir.shobeyri.ocr.source.dataSources.Api
import ir.shobeyri.ocr.source.repo.CameraRepo
import ir.shobeyri.ocr.source.repo.LibraryRepo
import ir.shobeyri.ocr.source.imp.CameraRepoImp
import ir.shobeyri.ocr.source.imp.LibraryRepoImp
import ir.shobeyri.ocr.utils.Constant.DB_NAME
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object Module {

    @Provides
    @Singleton
    fun provideRetrofit() : Api =
        Retrofit.Builder()
            .baseUrl("https://letsenvision.app/api/")
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(/*GsonBuilder().setLenient().create()*/))
            .client(OkHttpClient.Builder().addInterceptor(object : Interceptor {
                override fun intercept(chain: Interceptor.Chain): Response {
                    return chain.proceed(chain.request().newBuilder()
                        .addHeader("Cookie","__cfduid=d97604b6c67574ccd048c013ffbee703a1614774197").build())
                }

            }).addInterceptor(
                CurlLoggerInterceptor("CURL LOGGER") // I had an error so I have to check the curl
            ).build())
            .build()
            .create(Api::class.java)

    @Provides
    fun provideCameraRepo(api : Api) : CameraRepo = CameraRepoImp(api)

    @Provides
    fun provideLibraryRepo(dao : DaoInterface) : LibraryRepo = LibraryRepoImp(dao)

    @Singleton
    @Provides
    fun provideDb(@ApplicationContext context: Context) : Db = Room.databaseBuilder(context,
        Db::class.java,DB_NAME).build()

    @Singleton
    @Provides
    fun provideDao(db : Db) : DaoInterface = db.dao()

}