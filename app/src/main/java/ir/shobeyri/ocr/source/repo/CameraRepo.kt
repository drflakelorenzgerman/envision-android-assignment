package ir.shobeyri.ocr.source.repo

import androidx.lifecycle.LiveData
import io.reactivex.Single
import ir.shobeyri.ocr.data.ReadDocumentModel
import java.io.File

interface CameraRepo {
    fun readDocument(file : File): Single<ReadDocumentModel>
}