package ir.shobeyri.ocr.source.imp

import io.reactivex.Flowable
import ir.shobeyri.ocr.data.Response
import ir.shobeyri.ocr.source.db.DaoInterface
import ir.shobeyri.ocr.source.repo.LibraryRepo
import javax.inject.Inject

class LibraryRepoImp @Inject constructor(var dao: DaoInterface) : LibraryRepo {

    override fun getAllResponse(): Flowable<List<Response>> = dao.getAllResponse()

    override fun insertResponse(model: Response) {
        dao.insertResponse(model)
    }
}