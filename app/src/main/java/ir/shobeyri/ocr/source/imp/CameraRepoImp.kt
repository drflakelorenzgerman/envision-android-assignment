package ir.shobeyri.ocr.source.imp

import io.reactivex.Single
import ir.shobeyri.ocr.data.ReadDocumentModel
import ir.shobeyri.ocr.source.dataSources.Api
import ir.shobeyri.ocr.source.repo.CameraRepo
import ir.shobeyri.ocr.utils.createMultipartBody
import java.io.File
import javax.inject.Inject

class CameraRepoImp @Inject constructor(val api : Api) : CameraRepo {

    override fun readDocument(file: File): Single<ReadDocumentModel> =
        api.readDocument(file.createMultipartBody("image/*"))

}