package ir.shobeyri.ocr.source.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import ir.shobeyri.ocr.data.Response
import ir.shobeyri.ocr.utils.Constant.TABLE_Response

@Dao
interface  DaoInterface {
    @Insert
    fun insertResponse(model : Response)

    @Query("SELECT * FROM ${TABLE_Response}")
    fun getAllResponse() : Flowable<List<Response>>

}