package ir.shobeyri.ocr.source.dataSources

import io.reactivex.Single
import ir.shobeyri.ocr.data.ReadDocumentModel
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface Api {
    @Multipart
    @POST("test/readDocument")
    fun readDocument(
        @Part  file : MultipartBody.Part
    ) : Single<ReadDocumentModel>

}