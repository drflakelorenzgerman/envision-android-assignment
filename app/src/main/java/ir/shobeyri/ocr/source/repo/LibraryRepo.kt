package ir.shobeyri.ocr.source.repo

import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import ir.shobeyri.ocr.data.ReadDocumentModel
import ir.shobeyri.ocr.data.Response

interface LibraryRepo {
    fun getAllResponse(): Flowable<List<Response>>

    fun insertResponse(model: Response)
}