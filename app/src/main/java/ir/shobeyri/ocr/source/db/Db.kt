package ir.shobeyri.ocr.source.db

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import ir.shobeyri.ocr.data.ParagraphConverters
import ir.shobeyri.ocr.data.Response
import ir.shobeyri.ocr.source.db.DaoInterface

@Database(entities = [Response::class],version = 1)
@TypeConverters(ParagraphConverters::class)
abstract class Db : RoomDatabase(){
    abstract fun dao() : DaoInterface
}