package ir.shobeyri.ocr.source

import java.lang.Exception


/**
 * This is GeneralDataState class which is a State manager
 * We can have 4 stated :
 * 1.Response : some response from data source
 * 2.Failed : request to get rResponse get failed
 * 3.Loading : request to get response is began and it's not finished yet
 * 4.Nothing : none of 3 other states
 *
 * @author Ali Shobeyri
 *
 * @param data is the value of response , we use data in Failed and Loading for further features too but there is no data for Nothing
 * @param tag is a simple tag name and by default is the name of the state
 * @param error is belong to Failed state which show us what is the problem
 * @param message in Loading is what text we want to show during the Loading
 */
sealed class GeneralDataState<out ViewState>{

    data class Response<ViewState>(val data : ViewState?,val tag : String = "Response") : GeneralDataState<ViewState>()
    data class Failed<ViewState>(val data : ViewState?,val error : Exception,val tag : String = "Failed") : GeneralDataState<ViewState>()
    data class Loading<ViewState>(val data : ViewState?,val message : String,val tag : String = "Loading") : GeneralDataState<ViewState>()
    class Nothing<ViewState>(val tag : String = "Nothing") : GeneralDataState<ViewState>()

    /**
     * @return this function will return the data (could be null)
     */
    fun toData() : ViewState? = when(this){
        is  Response-> this.data
        is  Failed-> this.data
        is  Loading-> this.data
        else -> null
    }

    /**
     * @return this function will return the type which is name of state
     */
    fun type() : String = when(this){
        is  Response-> "Response"
        is  Failed-> "Failed"
        is  Loading-> "Loading"
        else -> "Cleared"
    }

    /**
     * @return this function will return the tag , by default it would be name of state
     */
    fun tag() : String = when(this){
        is  Response-> tag
        is  Failed-> tag
        is  Loading-> tag
        is  Nothing -> tag
    }
}

