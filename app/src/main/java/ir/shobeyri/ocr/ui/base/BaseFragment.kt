package ir.shobeyri.yara.ui.base

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.ui.MainActivity


/**
 *
 * This is our BaseFragment , every fragment should be extended from this class
 *
 * @author Ali Shobeyri
 *
 * @param DB is type of DataBinding would be attached to fragment
 * @param dataBinding is dataBinding , we can access our UI via this object
 * @param layout is the layout and dataBinding will be built based on this
 *
 */
abstract class  BaseFragment<DB : ViewDataBinding> : Fragment(){

    abstract val layout : Int
    lateinit var dataBinding : DB

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dataBinding = DataBindingUtil.inflate(inflater,layout,container,false)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding()
    }

    fun loading(on : Boolean,message : String = ""){
        (requireActivity() as MainActivity).loading(on,message)
    }

    fun createSnackBar(message: String,actionText : String,contentDes : String,action : () -> Unit){
        (requireActivity() as MainActivity).createSnackBar(message,actionText,contentDes,action)
    }

    fun getParentNavController() : NavController? = childFragmentManager.fragments.first()?.findNavController()

    /**
     * @return this function would return parent fragment, if there'll be a problem it will return null
     */
    inline fun <reified T : Fragment> getTheParentFragment() : T? {
        parentFragment?.let {
            val navHostFragment = it as NavHostFragment?
            navHostFragment?.let {
                if(it.parentFragment is T){
                    return (it.parentFragment as T)
                }
            }
        }
        return null
    }

    /**
     * @return this function would return Activity, if there'll be a problem it will return null
     * the difference of this function and requireActivity is this function use a Generic to specify the
     * exact type of Activity
     */
    inline fun <reified T : Activity> getTheActivity() : T?{
        if(requireActivity() is T) return requireActivity() as T
        return null
    }

    /**
     * this function would override onBackPressed
     * at most of the times you don't need it (if you handle with Navigation Component)
     * sometimes when you have nested Navigation Component or ... you may need it
     */
    fun handleBackPress(callback : ()->Unit) {
        val backListener = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                this.remove()
                callback()
            }

        }
        requireActivity().onBackPressedDispatcher.addCallback(backListener)
    }


    /**
     * a function which can be a replacement to onViewCreated()
     */
    abstract fun binding()
}
