package ir.shobeyri.yara.ui.base

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<DB : ViewDataBinding, DataType> (var list : MutableList<DataType>, var layout : Int) : RecyclerView.Adapter<BaseAdapterVH<DB>>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseAdapterVH<DB> =
        BaseAdapterVH(
            DataBindingUtil.inflate(LayoutInflater.from(parent.context), layout, parent, false)
        )

    override fun onBindViewHolder(
        holder: BaseAdapterVH<DB>,
        position: Int
    ) {
        bind(holder.binding,list[position],position)
    }

    override fun getItemCount(): Int = list.size

    abstract fun bind(binding : DB, item : DataType,position: Int)

}

class BaseAdapterVH<DB : ViewDataBinding>(var binding : DB) : RecyclerView.ViewHolder(binding.root){

}