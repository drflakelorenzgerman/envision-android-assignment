package ir.shobeyri.ocr.ui.capture.capture

import ir.shobeyri.ocr.data.ReadDocumentModel
import ir.shobeyri.ocr.data.Response
import java.io.File

sealed class CaptureFragmentIntent {
    class ReadDocument(val file : File) : CaptureFragmentIntent()
    class NavigateToResponseFragment(val data : Response) : CaptureFragmentIntent()
}