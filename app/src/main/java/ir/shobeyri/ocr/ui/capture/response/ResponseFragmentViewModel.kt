package ir.shobeyri.ocr.ui.capture.response

import dagger.hilt.android.lifecycle.HiltViewModel
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.source.repo.LibraryRepo
import ir.shobeyri.yara.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ResponseFragmentViewModel @Inject constructor(var repo : LibraryRepo)
    : BaseViewModel<ResponseFragmentViewState, ResponseFragmentIntent>() {

    override fun action(intent: ResponseFragmentIntent) {
        when(intent){
            is ResponseFragmentIntent.InsertNewResponse->{
                CoroutineScope(IO).launch {
                    repo.insertResponse(intent.model)
                    _dataStates.postValue(GeneralDataState.Response(
                        ResponseFragmentViewState()
                    ))
                }
            }

            is ResponseFragmentIntent.NavigateToLibrary->{
            }
        }
    }

}