package ir.shobeyri.ocr.ui.main

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.FragmentMainBinding
import ir.shobeyri.ocr.ui.capture.mother.MotherCaptureFragment
import ir.shobeyri.ocr.ui.library.mother.MotherLibraryFragment
import ir.shobeyri.yara.ui.base.BaseFragment

class MainFragment : BaseFragment<FragmentMainBinding>() {
    override val layout: Int
        get() = R.layout.fragment_main

    override fun binding() {
        val adapter = object : FragmentStateAdapter(requireActivity()) {
            override fun getItemCount(): Int = 2

            override fun createFragment(position: Int): Fragment =
                if(position == 0) MotherCaptureFragment.newInstance(){
                    this@MainFragment.navigateToLibrary()
                }
                else MotherLibraryFragment.newInstance()
        }



        dataBinding.pager.adapter = adapter

        TabLayoutMediator(dataBinding.tabs,dataBinding.pager){tab,pos->
            tab.text = if(pos == 0) getString(R.string.capture) else getString(R.string.library)
            tab.contentDescription = if(pos == 0) getString(R.string.content_capture_tab) else getString(
                            R.string.content_navigate_library)
        }.attach()
    }

    fun navigateToLibrary() {
        dataBinding.pager.currentItem = 1
    }
}