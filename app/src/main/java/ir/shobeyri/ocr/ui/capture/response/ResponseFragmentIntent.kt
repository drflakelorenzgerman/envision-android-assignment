package ir.shobeyri.ocr.ui.capture.response

import ir.shobeyri.ocr.data.ReadDocumentModel
import ir.shobeyri.ocr.data.Response
import java.io.File

sealed class ResponseFragmentIntent {
    class InsertNewResponse(var model: Response) : ResponseFragmentIntent()
    object NavigateToLibrary : ResponseFragmentIntent()
}
