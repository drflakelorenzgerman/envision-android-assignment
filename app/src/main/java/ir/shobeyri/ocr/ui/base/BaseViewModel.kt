package ir.shobeyri.yara.ui.base


import androidx.lifecycle.*
import ir.shobeyri.ocr.source.GeneralDataState
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.Main


/**
 *
 * This is our BaseViewModel , every viewModel should be extended from this class
 *
 * @author Ali Shobeyri
 *
 * @param _dataStates is MutableLiveData which keeps our DataStates , it must be protected so nothing except viewModels can't set and post value to it
 * @param dataStates is refrence to _dataStates , it's LiveData so nobody can't change it
 * @param T is ViewState Generic
 * @param Intent is Intent Generic , a sealed class which has different action
 *
 * @property action will take an Intent and decide what it should do next
 *
 * @property clearDataState will clear the satat to Nothing
 *
 * @property runRequest is a function which takes a lambda, this lambda will be invoked inside a Main Scope
 */
abstract class BaseViewModel<T,Intent> : ViewModel() {

    protected val _dataStates: MutableLiveData<GeneralDataState<T>> = MutableLiveData()
    val dataStates: LiveData<GeneralDataState<T>> = _dataStates

    protected fun getViewState() : T? = _dataStates.value?.toData()

    abstract fun action(intent : Intent)

    fun clearDataState(){
        _dataStates.value = GeneralDataState.Nothing()
    }

    fun manualLoading(message: String = "Loading...",nameTag : String = ""){
        _dataStates.postValue(GeneralDataState.Loading(getViewState(),message,nameTag))
    }

    fun manualStopLoading(nameTag: String = ""){
        _dataStates.postValue(GeneralDataState.Nothing(nameTag))
    }

    fun runRequest(nameTag : String = "",req : suspend() -> Unit) {
        CoroutineScope(Main).launch {
            val currentState = getViewState()
            try {
                _dataStates.postValue(GeneralDataState.Loading(currentState,"Loading...",nameTag))
                req()
            }catch (exception: Exception) {
                _dataStates.postValue(GeneralDataState.Failed(currentState, exception,nameTag))
            }
        }
    }

    fun setError(message : String){
        _dataStates.postValue(
            GeneralDataState.Failed(
                null, Exception(message)
            ))
    }

}
