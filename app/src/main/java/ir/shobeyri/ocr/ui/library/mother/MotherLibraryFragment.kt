package ir.shobeyri.ocr.ui.library.mother

import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.FragmentMotherLibraryBinding
import ir.shobeyri.yara.ui.base.BaseFragment

class MotherLibraryFragment  : BaseFragment<FragmentMotherLibraryBinding>() {
    override val layout: Int
        get() = R.layout.fragment_mother_library

    companion object{
        fun newInstance(): MotherLibraryFragment {
            return MotherLibraryFragment()
        }
    }

    override fun binding() {
        // nothing to do
    }
}