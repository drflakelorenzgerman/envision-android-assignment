package ir.shobeyri.ocr.ui.library.list

import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.Disposable
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.source.repo.LibraryRepo
import ir.shobeyri.yara.ui.base.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LibraryFragmentViewModel @Inject constructor(var repo : LibraryRepo)
    : BaseViewModel<LibraryFragmentViewState, LibraryFragmentIntent>() {

    var allResponseDisposable : Disposable? = null

    override fun action(intent: LibraryFragmentIntent) {
        when(intent){
            is LibraryFragmentIntent.GetTheLocalResponses->{
                CoroutineScope(Dispatchers.IO).launch {
                    allResponseDisposable = repo.getAllResponse().subscribe({
                        _dataStates.postValue(
                            GeneralDataState.Response(
                                LibraryFragmentViewState(it)
                            ))
                    },{
                        setError(it.message.toString())
                    })

                }
            }
        }
    }

    override fun onCleared() {
        allResponseDisposable?.dispose()
    }
}