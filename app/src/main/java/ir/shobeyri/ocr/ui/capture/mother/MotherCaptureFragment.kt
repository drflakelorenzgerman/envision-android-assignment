package ir.shobeyri.ocr.ui.capture.mother

import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.FragmentMotherBinding
import ir.shobeyri.yara.ui.base.BaseFragment

class MotherCaptureFragment : BaseFragment<FragmentMotherBinding>() {

    companion object{
        fun newInstance(navigateToLibrary : (() -> Unit)? = null): MotherCaptureFragment {
            return MotherCaptureFragment().apply {
                this.navigateToLibrary = navigateToLibrary
            }
        }
    }

    override val layout: Int
        get() = R.layout.fragment_mother

    var navigateToLibrary : (() -> Unit)? = null

    override fun binding() {
        // nothing to do
    }

    fun navigateToLibrary() {
        navigateToLibrary?.invoke()
    }
}