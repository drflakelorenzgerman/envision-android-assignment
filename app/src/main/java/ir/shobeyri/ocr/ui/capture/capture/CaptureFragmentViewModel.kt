package ir.shobeyri.ocr.ui.capture.capture

import androidx.lifecycle.LiveData
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.disposables.Disposable
import ir.shobeyri.ocr.source.repo.CameraRepo
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.source.subscribeOnIOAndObserveOnMainForSingle
import ir.shobeyri.ocr.ui.base.SingleLiveEvent
import ir.shobeyri.yara.ui.base.BaseViewModel
import java.io.File
import javax.inject.Inject

@HiltViewModel
class CaptureFragmentViewModel @Inject constructor(var repo : CameraRepo)
    : BaseViewModel<CaptureFragmentViewState, CaptureFragmentIntent>() {

    private val _viewEffects: SingleLiveEvent<CaptureFragmentViewEffect> = SingleLiveEvent()
    val viewEffects: LiveData<CaptureFragmentViewEffect> = _viewEffects
    var readDocumentDisposable : Disposable? = null

    override fun action(intent: CaptureFragmentIntent) {
        when(intent){
            is CaptureFragmentIntent.ReadDocument->{
                readDocument(intent.file)
            }
            is CaptureFragmentIntent.NavigateToResponseFragment->{
                clearDataState()
                _viewEffects.value = CaptureFragmentViewEffect.NavigateToResponseFragment(intent.data)
            }
        }
    }

    private fun readDocument(file: File) {
        runRequest {
            readDocumentDisposable = repo.readDocument(file).subscribeOnIOAndObserveOnMainForSingle().subscribe({
                _dataStates.postValue(GeneralDataState.Response(
                    CaptureFragmentViewState(it)
                ))
            },{
                setError(it.message.toString())
            })
        }
    }

    override fun onCleared() {
        readDocumentDisposable?.dispose()
    }
}