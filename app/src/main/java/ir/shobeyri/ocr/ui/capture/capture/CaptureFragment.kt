package ir.shobeyri.ocr.ui.capture.capture

import android.Manifest
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.FragmentCaptureBinding
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.utils.Permission
import ir.shobeyri.ocr.utils.delay
import ir.shobeyri.ocr.utils.toast
import ir.shobeyri.yara.ui.base.BaseFragment
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@AndroidEntryPoint
class CaptureFragment : BaseFragment<FragmentCaptureBinding>() {
    override val layout: Int
        get() = R.layout.fragment_capture

    private lateinit var cameraExecutor: ExecutorService
    private var imageCapture : ImageCapture? = null
    val viewModel by viewModels<CaptureFragmentViewModel>()

    override fun binding() {

        setCamera()

        viewModel.viewEffects.observe(viewLifecycleOwner, Observer {
            when(it){
                is CaptureFragmentViewEffect.NavigateToResponseFragment->{
                    findNavController().navigate(CaptureFragmentDirections.actionCaptureFragmentToResponseFragment(it.data))
                }
            }
        } )

        viewModel.dataStates.observe(viewLifecycleOwner, Observer {
            when(it){
                is GeneralDataState.Failed->{
                    requireContext().toast(getString(R.string.error_prefix)+it.error.localizedMessage)
                    loading(false)
                }
                is GeneralDataState.Loading->{
                    loading(true,getString(R.string.ocr_in_progess))
                }
                is GeneralDataState.Nothing->{
                    loading(false)
                }
                is GeneralDataState.Response->{
                    loading(false)
                    it.data?.readDocument?.let {
                        val flag = it.response?.let {
                        viewModel.action(CaptureFragmentIntent.NavigateToResponseFragment(it))
                            true
                        }?: false
                        if(!flag)
                            requireContext().toast(it.message)

                    }
                }
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if(askAgainAboutPermission)
            setCamera()

        askAgainAboutPermission = true
    }

    private var askAgainAboutPermission = true
    private fun setCamera(){
        Permission.getRunTimePermission(requireActivity(), Manifest.permission.CAMERA){
            askAgainAboutPermission = false
            if(!it){
                requireContext().toast(getString(R.string.no_permission))
                viewModel.manualStopLoading()
            }else{
                cameraExecutor = Executors.newSingleThreadExecutor()
                startCamera()
            }
        }
    }

    private fun startCamera() {

        dataBinding.btnCapture.setOnClickListener {
            askAgainAboutPermission = false
            Permission.getRunTimePermission(requireActivity(),Manifest.permission.WRITE_EXTERNAL_STORAGE){
                if(it){
                    takePhoto()
                }else{
                    requireContext().toast(getString(R.string.no_permission))
                    viewModel.manualStopLoading()
                }
            }
        }

        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(dataBinding.preview.surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    viewLifecycleOwner, cameraSelector, preview,imageCapture)

            } catch(exc: Exception) {
                requireContext().toast(exc.localizedMessage)
            }

        }, ContextCompat.getMainExecutor(requireContext()))
    }

    private fun takePhoto() {
        viewModel.manualLoading(getString(R.string.ocr_in_progess))
        // Get a stable reference of the modifiable image capture use case
        val imageCapture = imageCapture ?: return

        // Create time-stamped output file to hold the image
        val photoFile = File(
            requireContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES),
            SimpleDateFormat("yyyy-mm-dd", Locale.US
            ).format(System.currentTimeMillis()) + ".jpg")

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(
            outputOptions, ContextCompat.getMainExecutor(requireContext()), object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    requireContext().toast(exc.localizedMessage)
                    viewModel.manualStopLoading()
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    viewModel.action(CaptureFragmentIntent.ReadDocument(photoFile))
                }
            })
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }
}