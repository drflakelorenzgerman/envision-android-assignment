package ir.shobeyri.ocr.ui.capture.capture

import ir.shobeyri.ocr.data.Response

sealed class CaptureFragmentViewEffect {
    class NavigateToResponseFragment(val data : Response) : CaptureFragmentViewEffect()
}