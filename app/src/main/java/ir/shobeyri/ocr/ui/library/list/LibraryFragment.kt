package ir.shobeyri.ocr.ui.library.list

import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.data.Response
import ir.shobeyri.ocr.databinding.FragmentLibraryBinding
import ir.shobeyri.ocr.databinding.ItemResponseBinding
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.utils.toast
import ir.shobeyri.yara.ui.base.BaseAdapter
import ir.shobeyri.yara.ui.base.BaseFragment

@AndroidEntryPoint
class LibraryFragment : BaseFragment<FragmentLibraryBinding>() {
    override val layout: Int
        get() = R.layout.fragment_library

    val viewModel by viewModels<LibraryFragmentViewModel>()

    override fun binding() {

        createAdapter()

        viewModel.dataStates.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when(it){
                is GeneralDataState.Failed->{
                    requireContext().toast(getString(R.string.error_prefix)+it.error.localizedMessage)
                    loading(false)
                }
                is GeneralDataState.Loading->{
                    loading(true,getString(R.string.loading_prefix)) // wouldn't happen
                }
                is GeneralDataState.Nothing->{
                    loading(false)
                }
                is GeneralDataState.Response->{
                    it.data?.data?.let {
                        addElementToAdapter(it.toMutableList())
                    }
                }
            }
        })

        viewModel.action(LibraryFragmentIntent.GetTheLocalResponses)

    }

    lateinit var adapter : BaseAdapter<ItemResponseBinding,Response>
    fun createAdapter(){
        adapter = object : BaseAdapter<ItemResponseBinding,Response>(mutableListOf(),R.layout.item_response){
            override fun bind(
                binding: ItemResponseBinding,
                item: Response,
                position: Int
            ) {
                binding.date = item.date
                binding.content = getString(R.string.content_item_local)+item.date

                binding.root.setOnClickListener {
                    findNavController().navigate(LibraryFragmentDirections.actionLibraryFragmentToResponseFragment(item,false))
                }
            }
        }
        dataBinding.recycler.adapter = adapter
        dataBinding.recycler.layoutManager = LinearLayoutManager(requireContext())
    }

    fun addElementToAdapter(list : MutableList<Response>){
        adapter.list.clear()
        adapter.list.addAll(list)
        adapter.notifyDataSetChanged()
    }
}