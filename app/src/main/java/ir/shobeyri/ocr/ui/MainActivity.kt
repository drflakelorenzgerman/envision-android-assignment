package ir.shobeyri.ocr.ui

import android.view.View
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.ActivityMainBinding
import ir.shobeyri.ocr.ui.loading.LoadingDialog
import ir.shobeyri.ocr.utils.active
import ir.shobeyri.ocr.utils.delay
import ir.shobeyri.yara.ui.base.BaseActivity

@AndroidEntryPoint
class MainActivity : BaseActivity<ActivityMainBinding>() {
    override val layout: Int
        get() = R.layout.activity_main

    var loadingDialog : LoadingDialog? = null

    override fun binding() {
    }

    override fun loading(on: Boolean,message : String) {
        if(on){
            if(loadingDialog == null)
                loadingDialog = LoadingDialog(message,this)
            loadingDialog?.show()
        }
        else
            loadingDialog?.dismiss()
    }

    override fun createSnackBar(message: String, actionText: String,contentDes : String, action: () -> Unit) {
        dataBinding.linSnackbar.visibility = View.VISIBLE
        dataBinding.message = message
        dataBinding.content = contentDes
        dataBinding.actionText = actionText
        dataBinding.btnAction.active(true)

        dataBinding.linSnackbar.animate()
            .alpha(1f).duration = 300

        dataBinding.btnAction.setOnClickListener {
            action.invoke()
            dataBinding.btnAction.active(false)
        }

        removeSnackBar()
    }

    private fun removeSnackBar(){
        delay(5000){
            dataBinding.linSnackbar.animate()
                .alpha(0f).duration = 300
            delay(300){
                dataBinding.linSnackbar.visibility = View.GONE
            }
        }
    }

}