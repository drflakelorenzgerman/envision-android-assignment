package ir.shobeyri.ocr.ui.library.list

import ir.shobeyri.ocr.data.Response


class LibraryFragmentViewState(
    val data : List<Response>?
)