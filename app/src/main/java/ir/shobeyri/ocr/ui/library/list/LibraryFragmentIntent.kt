package ir.shobeyri.ocr.ui.library.list

sealed class LibraryFragmentIntent {
    object GetTheLocalResponses : LibraryFragmentIntent()
}