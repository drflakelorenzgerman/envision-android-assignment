package ir.shobeyri.ocr.ui.loading

import android.content.Context
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.DialogLoadingBinding
import ir.shobeyri.ocr.ui.base.BaseDialog

class LoadingDialog (var message : String,context: Context) : BaseDialog<DialogLoadingBinding>(false,context) {
    override val layout: Int
        get() = R.layout.dialog_loading

    override fun binding() {
        dataBinding.message = message
        dataBinding.content = context.getString(R.string.loading_content)+message

    }
}