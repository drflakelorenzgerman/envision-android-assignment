package ir.shobeyri.ocr.ui.capture.response

import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.R
import ir.shobeyri.ocr.databinding.FragmentResponseBinding
import ir.shobeyri.ocr.source.GeneralDataState
import ir.shobeyri.ocr.ui.capture.mother.MotherCaptureFragment
import ir.shobeyri.ocr.utils.active
import ir.shobeyri.ocr.utils.radiusStrokeBackground
import ir.shobeyri.ocr.utils.toast
import ir.shobeyri.yara.ui.base.BaseFragment
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class ResponseFragment : BaseFragment<FragmentResponseBinding>() {
    override val layout: Int
        get() = R.layout.fragment_response

    val args by navArgs<ResponseFragmentArgs>()
    val viewModel by viewModels<ResponseFragmentViewModel>()

    override fun binding() {

        val sdf = SimpleDateFormat("dd/MM/yy HH:mm")
        args.data.date = sdf.format(Calendar.getInstance().time)

        var str = ""
        args.data.paragraphs.forEach {
            str += it.paragraph + " "
        }
        dataBinding.data = str


        if(args.enableBtn){
            dataBinding.btnSave.setOnClickListener {
            viewModel.action(ResponseFragmentIntent.InsertNewResponse(args.data))
        }
        }else{
            dataBinding.btnSave.visibility = View.GONE
        }



        handleBackPress{
            findNavController().popBackStack()
        }

        viewModel.dataStates.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            when(it){
                is GeneralDataState.Failed->{
                    requireContext().toast(getString(R.string.error_prefix)+it.error.localizedMessage)
                    loading(false)
                }
                is GeneralDataState.Loading->{
                    loading(true,getString(R.string.loading_prefix)) // wouldn't happen
                }
                is GeneralDataState.Nothing->{
                    loading(false)
                }
                is GeneralDataState.Response->{
                    dataBinding.btnSave.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.gray))
                    radiusStrokeBackground(dataBinding.btnSave,"50",0,0)
                    dataBinding.btnSave.active(false)

                    createSnackBar(getString(R.string.text_saved_to_library),
                        getString(R.string.go_to_library),
                        getString(R.string.save_to_library_des)){
                        getTheParentFragment<MotherCaptureFragment>()?.navigateToLibrary()
                        findNavController().popBackStack()
                    }

                }
            }
        })
    }
}