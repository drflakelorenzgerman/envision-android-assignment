package ir.shobeyri.yara.ui.base

import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.os.Bundle
import android.os.Message
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import dagger.hilt.android.AndroidEntryPoint
import ir.shobeyri.ocr.utils.Permission

/**
 *
 * This is our BaseActivity , every activity (our app is single activity right now) should be extended from this class
 *
 * @author Ali Shobeyri
 *
 * @param DB is type of DataBinding would be attached to activity
 * @param dataBinding is dataBinding , we can access our UI via this object
 * @param layout is the layout and dataBinding will be built based on this
 *
 */
abstract class BaseActivity<DB : ViewDataBinding> : AppCompatActivity() {
    abstract val layout : Int
    lateinit var dataBinding : DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(this),layout,null,false)
        setContentView(dataBinding.root)
        binding()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == Permission.code && !grantResults.isEmpty() && !permissions.isEmpty()) // controller so nobody except us won't request a permission
            Permission.permissionGenerated(permissions[0],grantResults[0] == PERMISSION_GRANTED)
    }

    abstract fun binding()

    abstract fun loading(on: Boolean,message: String = "")

    abstract fun createSnackBar(message: String,actionText : String,contentDes : String,action : () -> Unit)
}