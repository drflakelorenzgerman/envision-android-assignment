package ir.shobeyri.ocr.ui.capture.capture

import ir.shobeyri.ocr.data.ReadDocumentModel

class CaptureFragmentViewState(
    val readDocument : ReadDocumentModel?
)