package ir.shobeyri.ocr.ui.base

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

abstract class BaseDialog<DB : ViewDataBinding>(var canelable : Boolean,context: Context) : Dialog(context) {

    abstract val layout : Int
    lateinit var dataBinding : DB

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(context),layout,null,false)
        setContentView(dataBinding.root)
        binding()

        if(!canelable){
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }
    }

    abstract fun binding()
}