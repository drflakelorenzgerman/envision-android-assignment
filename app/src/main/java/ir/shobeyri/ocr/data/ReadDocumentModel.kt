package ir.shobeyri.ocr.data

import android.os.Parcelable
import androidx.room.*
import com.google.gson.Gson
import ir.shobeyri.ocr.utils.Constant.TABLE_Response
import kotlinx.parcelize.Parcelize


class ReadDocumentModel(
    val message : String,
    val response: Response?,
)


class ParagraphConverters {
    companion object{
        @TypeConverter
        @JvmStatic
        fun stringToSearch(value: String): List<Paragraph>
                = Gson().fromJson<Array<Paragraph>>(value,Array<Paragraph>::class.java).toList()

        @JvmStatic
        @TypeConverter
        fun searchToString(list: List<Paragraph>): String = Gson().toJson(list.toTypedArray())
    }
}

@Parcelize
@Entity(tableName = TABLE_Response)
data class Response(
    @TypeConverters(ParagraphConverters::class)
    val paragraphs: List<Paragraph>,
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    var date : String = ""
) : Parcelable

@Parcelize
data class Paragraph(
    val language: String,
    val paragraph: String
) : Parcelable
