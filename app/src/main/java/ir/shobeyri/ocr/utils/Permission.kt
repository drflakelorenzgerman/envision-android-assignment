package ir.shobeyri.ocr.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

/**
 * this object is a utility we use for getting permission
 * @param listOfActions is a map of String->Lambda , the lambda will called from permissionGenerated()
 *
 *
 * when you request for a permission this object will store an action,after that at BaseActivity.onRequestPermissionsResult()
 * it will call permissionGenerated() and by permissionType it will get the proper action
 *
 *
 * @author Ali Shobeyri
 */
object Permission {

    const val code = 1

    private var listOfActions = mutableMapOf<String,(Boolean) -> Unit>()

    fun getRunTimePermission(activity : Activity,permissionType : String,action : (permission : Boolean) -> Unit){
        if(ContextCompat.checkSelfPermission(activity, permissionType) == PackageManager.PERMISSION_GRANTED)
            action.invoke(true)
        else{
            listOfActions[permissionType] = action
            ActivityCompat.requestPermissions(activity, arrayOf(permissionType),code)
        }
    }

    fun permissionGenerated(permissionType: String,permission: Boolean){
        listOfActions[permissionType]?.invoke(permission)
    }
}