package ir.shobeyri.ocr.utils

import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

fun File.createMultipartBody(type: String) : MultipartBody.Part = MultipartBody.Part.createFormData("photo",this.name,
    RequestBody.create(MediaType.parse(type), this))
