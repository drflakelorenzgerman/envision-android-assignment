package ir.shobeyri.ocr.utils

object Constant {
    const val DEFAULT_TAG_GeneralDataState = "GeneralDataState"
    const val DB_NAME = "DB_NAME"
    const val TABLE_Response = "Response"
}