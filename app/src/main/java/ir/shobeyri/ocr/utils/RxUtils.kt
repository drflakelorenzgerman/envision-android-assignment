package ir.shobeyri.ocr.source

import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


fun <T> Single<T>.subscribeAndObserveOnIOForSingle() : Single<T>{
    return this.subscribeOn(Schedulers.io())
        .observeOn(Schedulers.io())
}



fun <T> Single<T>.subscribeOnIOAndObserveOnMainForSingle() : Single<T>{
    return this.subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}
