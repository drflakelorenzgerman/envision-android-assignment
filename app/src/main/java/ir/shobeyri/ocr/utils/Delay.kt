package ir.shobeyri.ocr.utils

import android.os.Handler
import android.os.Looper

fun delay(duration: Int, action : () -> Unit){
    Handler(Looper.getMainLooper()).postDelayed(Runnable {
        action.invoke()
    },duration.toLong())
}