package ir.shobeyri.ocr.utils

import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.view.View
import androidx.annotation.Nullable
import androidx.databinding.BindingAdapter


@BindingAdapter(
    value = ["ext:borderRadius", "ext:strokeColor", "ext:strokeDp"],
    requireAll = false
)
fun setRadius(view: View, radius: String?, stroke: Int = 0, dp: Int = 0) {
    radiusStrokeBackground(view, radius, stroke, dp)
}

fun radiusStrokeBackground(
    view: View, @Nullable radius: String?,
    stroke: Int,
    dp: Int
) {
    var shape = GradientDrawable()
    shape.shape = GradientDrawable.RECTANGLE
    var color = (view.background as? ColorDrawable?)?.color ?: Color.TRANSPARENT
    shape.setColor(color)
    (dp != 0).let {
        shape.setStroke(dp, stroke)
    }

    if (radius != null && radius.trim().isNotEmpty()) {
        val R = radius.split(",")
        var r1 = 0f
        var r2 = 0f
        var r3 = 0f
        var r4 = 0f
        r1 = convertDpToPx(view.context, R[0].toFloat()) as Float
        r2 = r1
        r3 = r1
        r4 = r1
        if (R.size > 1)
            r2 = convertDpToPx(view.context, R[1].toFloat()) as Float
        if (R.size > 2)
            r3 = convertDpToPx(view.context, R[2].toFloat()) as Float
        if (R.size > 3)
            r4 = convertDpToPx(view.context, R[3].toFloat()) as Float
        shape.cornerRadii = floatArrayOf(r2, r2, r1, r1, r4, r4, r3, r3)
        view.background = shape
        return
    }
    view.background = shape
}