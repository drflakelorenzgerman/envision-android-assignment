package ir.shobeyri.ocr.utils

import android.view.View

fun View.active(isActive : Boolean){
    this.isEnabled = isActive
}